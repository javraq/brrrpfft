﻿
namespace BrrrPfft_test
{
    public class BrrrPfftModel
    {
        /// <summary>
        /// getter/setter for CountFrom
        /// </summary>
        public int CountFrom { get; set; }
        /// <summary>
        /// getter/setter for CountTo
        /// </summary>
        public int CountTo { get; set; }
        /// <summary>
        /// getter/setter for Pfft
        /// </summary>
        public int Pfft { get; set; }
        /// <summary>
        /// getter/setter for Brrr
        /// </summary>
        public int Brrr { get; set; }
        /// <summary>
        /// getter/setter for BrrrPfft
        /// </summary>
        public int[] BrrrPfft { get; set; }

    }
}
