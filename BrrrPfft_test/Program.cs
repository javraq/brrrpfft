﻿using System;
using System.Linq;
using System.Configuration;

namespace BrrrPfft_test
{
    class Program
    {    
        static void Main()
        {
            BrrrPfftModel bp = new BrrrPfftModel();
            // Try to get all values from app.config file.
            try
            {
               
                bp.CountFrom = int.Parse(ConfigurationManager.AppSettings["CountFrom"]);
                bp.CountTo = int.Parse(ConfigurationManager.AppSettings["CountTo"]);
                bp.Pfft = int.Parse(ConfigurationManager.AppSettings["Pfft"]);
                bp.Brrr = int.Parse(ConfigurationManager.AppSettings["Brrr"]);
                bp.BrrrPfft = ConfigurationManager.AppSettings["BrrrPfft"].Split(',').Select(int.Parse).ToArray();
            }
            catch(FormatException)
            {
                Console.WriteLine("Please use a whole number in app.config");
                Environment.Exit(0);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            // Loop From CountFrom to CountTo and writes Pfft or Brrr if number Modules Brrr Or pfft equals zero. 
            for(int i = bp.CountFrom; i <= bp.CountTo; i++)
            {
                if(bp.BrrrPfft.All(item => i % item == 0))
                {
                    Console.WriteLine("BrrrPfft");
                }
                else if(i % bp.Pfft == 0)
                {
                    Console.WriteLine("Pfft");
                }
                else if (i % bp.Brrr == 0)
                {
                    Console.WriteLine("Brrr");
                }
                else
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}

